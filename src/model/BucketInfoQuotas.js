/**
 * Garage Administration API v0+garage-v0.9.0
 * Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* 
 *
 * The version of the OpenAPI document: v0.9.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The BucketInfoQuotas model module.
 * @module model/BucketInfoQuotas
 * @version v0.9.0
 */
class BucketInfoQuotas {
    /**
     * Constructs a new <code>BucketInfoQuotas</code>.
     * @alias module:model/BucketInfoQuotas
     */
    constructor() { 
        
        BucketInfoQuotas.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>BucketInfoQuotas</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BucketInfoQuotas} obj Optional instance to populate.
     * @return {module:model/BucketInfoQuotas} The populated <code>BucketInfoQuotas</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new BucketInfoQuotas();

            if (data.hasOwnProperty('maxSize')) {
                obj['maxSize'] = ApiClient.convertToType(data['maxSize'], 'Number');
            }
            if (data.hasOwnProperty('maxObjects')) {
                obj['maxObjects'] = ApiClient.convertToType(data['maxObjects'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} maxSize
 */
BucketInfoQuotas.prototype['maxSize'] = undefined;

/**
 * @member {Number} maxObjects
 */
BucketInfoQuotas.prototype['maxObjects'] = undefined;






export default BucketInfoQuotas;

