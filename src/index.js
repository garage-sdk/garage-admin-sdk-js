/**
 * Garage Administration API v0+garage-v0.9.0
 * Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* 
 *
 * The version of the OpenAPI document: v0.9.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from './ApiClient';
import AddKeyRequest from './model/AddKeyRequest';
import AddNode200ResponseInner from './model/AddNode200ResponseInner';
import AllowBucketKeyRequest from './model/AllowBucketKeyRequest';
import AllowBucketKeyRequestPermissions from './model/AllowBucketKeyRequestPermissions';
import ApplyLayout200Response from './model/ApplyLayout200Response';
import BucketInfo from './model/BucketInfo';
import BucketInfoQuotas from './model/BucketInfoQuotas';
import BucketInfoWebsiteConfig from './model/BucketInfoWebsiteConfig';
import BucketKeyInfo from './model/BucketKeyInfo';
import ClusterLayout from './model/ClusterLayout';
import CreateBucketRequest from './model/CreateBucketRequest';
import CreateBucketRequestLocalAlias from './model/CreateBucketRequestLocalAlias';
import CreateBucketRequestLocalAliasAllow from './model/CreateBucketRequestLocalAliasAllow';
import GetHealth200Response from './model/GetHealth200Response';
import GetNodes200Response from './model/GetNodes200Response';
import ImportKeyRequest from './model/ImportKeyRequest';
import KeyInfo from './model/KeyInfo';
import KeyInfoBucketsInner from './model/KeyInfoBucketsInner';
import KeyInfoBucketsInnerPermissions from './model/KeyInfoBucketsInnerPermissions';
import KeyInfoPermissions from './model/KeyInfoPermissions';
import LayoutVersion from './model/LayoutVersion';
import ListBuckets200ResponseInner from './model/ListBuckets200ResponseInner';
import ListBuckets200ResponseInnerLocalAliasesInner from './model/ListBuckets200ResponseInnerLocalAliasesInner';
import ListKeys200ResponseInner from './model/ListKeys200ResponseInner';
import NodeClusterInfo from './model/NodeClusterInfo';
import NodeNetworkInfo from './model/NodeNetworkInfo';
import NodeRoleChange from './model/NodeRoleChange';
import NodeRoleRemove from './model/NodeRoleRemove';
import NodeRoleUpdate from './model/NodeRoleUpdate';
import UpdateBucketRequest from './model/UpdateBucketRequest';
import UpdateBucketRequestQuotas from './model/UpdateBucketRequestQuotas';
import UpdateBucketRequestWebsiteAccess from './model/UpdateBucketRequestWebsiteAccess';
import UpdateKeyRequest from './model/UpdateKeyRequest';
import UpdateKeyRequestAllow from './model/UpdateKeyRequestAllow';
import UpdateKeyRequestDeny from './model/UpdateKeyRequestDeny';
import BucketApi from './api/BucketApi';
import KeyApi from './api/KeyApi';
import LayoutApi from './api/LayoutApi';
import NodesApi from './api/NodesApi';


/**
* Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* .<br>
* The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
* <p>
* An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
* <pre>
* var garage = require('index'); // See note below*.
* var xxxSvc = new garage.XxxApi(); // Allocate the API class we're going to use.
* var yyyModel = new garage.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
* and put the application logic within the callback function.</em>
* </p>
* <p>
* A non-AMD browser application (discouraged) might do something like this:
* <pre>
* var xxxSvc = new garage.XxxApi(); // Allocate the API class we're going to use.
* var yyy = new garage.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* </p>
* @module index
* @version v0.9.0
*/
export {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient,

    /**
     * The AddKeyRequest model constructor.
     * @property {module:model/AddKeyRequest}
     */
    AddKeyRequest,

    /**
     * The AddNode200ResponseInner model constructor.
     * @property {module:model/AddNode200ResponseInner}
     */
    AddNode200ResponseInner,

    /**
     * The AllowBucketKeyRequest model constructor.
     * @property {module:model/AllowBucketKeyRequest}
     */
    AllowBucketKeyRequest,

    /**
     * The AllowBucketKeyRequestPermissions model constructor.
     * @property {module:model/AllowBucketKeyRequestPermissions}
     */
    AllowBucketKeyRequestPermissions,

    /**
     * The ApplyLayout200Response model constructor.
     * @property {module:model/ApplyLayout200Response}
     */
    ApplyLayout200Response,

    /**
     * The BucketInfo model constructor.
     * @property {module:model/BucketInfo}
     */
    BucketInfo,

    /**
     * The BucketInfoQuotas model constructor.
     * @property {module:model/BucketInfoQuotas}
     */
    BucketInfoQuotas,

    /**
     * The BucketInfoWebsiteConfig model constructor.
     * @property {module:model/BucketInfoWebsiteConfig}
     */
    BucketInfoWebsiteConfig,

    /**
     * The BucketKeyInfo model constructor.
     * @property {module:model/BucketKeyInfo}
     */
    BucketKeyInfo,

    /**
     * The ClusterLayout model constructor.
     * @property {module:model/ClusterLayout}
     */
    ClusterLayout,

    /**
     * The CreateBucketRequest model constructor.
     * @property {module:model/CreateBucketRequest}
     */
    CreateBucketRequest,

    /**
     * The CreateBucketRequestLocalAlias model constructor.
     * @property {module:model/CreateBucketRequestLocalAlias}
     */
    CreateBucketRequestLocalAlias,

    /**
     * The CreateBucketRequestLocalAliasAllow model constructor.
     * @property {module:model/CreateBucketRequestLocalAliasAllow}
     */
    CreateBucketRequestLocalAliasAllow,

    /**
     * The GetHealth200Response model constructor.
     * @property {module:model/GetHealth200Response}
     */
    GetHealth200Response,

    /**
     * The GetNodes200Response model constructor.
     * @property {module:model/GetNodes200Response}
     */
    GetNodes200Response,

    /**
     * The ImportKeyRequest model constructor.
     * @property {module:model/ImportKeyRequest}
     */
    ImportKeyRequest,

    /**
     * The KeyInfo model constructor.
     * @property {module:model/KeyInfo}
     */
    KeyInfo,

    /**
     * The KeyInfoBucketsInner model constructor.
     * @property {module:model/KeyInfoBucketsInner}
     */
    KeyInfoBucketsInner,

    /**
     * The KeyInfoBucketsInnerPermissions model constructor.
     * @property {module:model/KeyInfoBucketsInnerPermissions}
     */
    KeyInfoBucketsInnerPermissions,

    /**
     * The KeyInfoPermissions model constructor.
     * @property {module:model/KeyInfoPermissions}
     */
    KeyInfoPermissions,

    /**
     * The LayoutVersion model constructor.
     * @property {module:model/LayoutVersion}
     */
    LayoutVersion,

    /**
     * The ListBuckets200ResponseInner model constructor.
     * @property {module:model/ListBuckets200ResponseInner}
     */
    ListBuckets200ResponseInner,

    /**
     * The ListBuckets200ResponseInnerLocalAliasesInner model constructor.
     * @property {module:model/ListBuckets200ResponseInnerLocalAliasesInner}
     */
    ListBuckets200ResponseInnerLocalAliasesInner,

    /**
     * The ListKeys200ResponseInner model constructor.
     * @property {module:model/ListKeys200ResponseInner}
     */
    ListKeys200ResponseInner,

    /**
     * The NodeClusterInfo model constructor.
     * @property {module:model/NodeClusterInfo}
     */
    NodeClusterInfo,

    /**
     * The NodeNetworkInfo model constructor.
     * @property {module:model/NodeNetworkInfo}
     */
    NodeNetworkInfo,

    /**
     * The NodeRoleChange model constructor.
     * @property {module:model/NodeRoleChange}
     */
    NodeRoleChange,

    /**
     * The NodeRoleRemove model constructor.
     * @property {module:model/NodeRoleRemove}
     */
    NodeRoleRemove,

    /**
     * The NodeRoleUpdate model constructor.
     * @property {module:model/NodeRoleUpdate}
     */
    NodeRoleUpdate,

    /**
     * The UpdateBucketRequest model constructor.
     * @property {module:model/UpdateBucketRequest}
     */
    UpdateBucketRequest,

    /**
     * The UpdateBucketRequestQuotas model constructor.
     * @property {module:model/UpdateBucketRequestQuotas}
     */
    UpdateBucketRequestQuotas,

    /**
     * The UpdateBucketRequestWebsiteAccess model constructor.
     * @property {module:model/UpdateBucketRequestWebsiteAccess}
     */
    UpdateBucketRequestWebsiteAccess,

    /**
     * The UpdateKeyRequest model constructor.
     * @property {module:model/UpdateKeyRequest}
     */
    UpdateKeyRequest,

    /**
     * The UpdateKeyRequestAllow model constructor.
     * @property {module:model/UpdateKeyRequestAllow}
     */
    UpdateKeyRequestAllow,

    /**
     * The UpdateKeyRequestDeny model constructor.
     * @property {module:model/UpdateKeyRequestDeny}
     */
    UpdateKeyRequestDeny,

    /**
    * The BucketApi service constructor.
    * @property {module:api/BucketApi}
    */
    BucketApi,

    /**
    * The KeyApi service constructor.
    * @property {module:api/KeyApi}
    */
    KeyApi,

    /**
    * The LayoutApi service constructor.
    * @property {module:api/LayoutApi}
    */
    LayoutApi,

    /**
    * The NodesApi service constructor.
    * @property {module:api/NodesApi}
    */
    NodesApi
};
