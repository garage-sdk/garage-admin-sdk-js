# garage.BucketInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**globalAliases** | **[String]** |  | [optional] 
**websiteAccess** | **Boolean** |  | [optional] 
**websiteConfig** | [**BucketInfoWebsiteConfig**](BucketInfoWebsiteConfig.md) |  | [optional] 
**keys** | [**[BucketKeyInfo]**](BucketKeyInfo.md) |  | [optional] 
**objects** | **Number** |  | [optional] 
**bytes** | **Number** |  | [optional] 
**unfinishedUploads** | **Number** |  | [optional] 
**quotas** | [**BucketInfoQuotas**](BucketInfoQuotas.md) |  | [optional] 


