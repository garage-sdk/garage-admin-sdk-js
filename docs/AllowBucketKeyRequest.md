# garage.AllowBucketKeyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bucketId** | **String** |  | 
**accessKeyId** | **String** |  | 
**permissions** | [**AllowBucketKeyRequestPermissions**](AllowBucketKeyRequestPermissions.md) |  | 


