/**
 * Garage Administration API v0+garage-v0.8.0
 * Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* 
 *
 * The version of the OpenAPI document: v0.8.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.garage);
  }
}(this, function(expect, garage) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new garage.UpdateBucketRequestWebsiteAccess();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('UpdateBucketRequestWebsiteAccess', function() {
    it('should create an instance of UpdateBucketRequestWebsiteAccess', function() {
      // uncomment below and update the code to test UpdateBucketRequestWebsiteAccess
      //var instance = new garage.UpdateBucketRequestWebsiteAccess();
      //expect(instance).to.be.a(garage.UpdateBucketRequestWebsiteAccess);
    });

    it('should have the property enabled (base name: "enabled")', function() {
      // uncomment below and update the code to test the property enabled
      //var instance = new garage.UpdateBucketRequestWebsiteAccess();
      //expect(instance).to.be();
    });

    it('should have the property indexDocument (base name: "indexDocument")', function() {
      // uncomment below and update the code to test the property indexDocument
      //var instance = new garage.UpdateBucketRequestWebsiteAccess();
      //expect(instance).to.be();
    });

    it('should have the property errorDocument (base name: "errorDocument")', function() {
      // uncomment below and update the code to test the property errorDocument
      //var instance = new garage.UpdateBucketRequestWebsiteAccess();
      //expect(instance).to.be();
    });

  });

}));
