# garage.UpdateBucketRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**websiteAccess** | [**UpdateBucketRequestWebsiteAccess**](UpdateBucketRequestWebsiteAccess.md) |  | [optional] 
**quotas** | [**UpdateBucketRequestQuotas**](UpdateBucketRequestQuotas.md) |  | [optional] 


