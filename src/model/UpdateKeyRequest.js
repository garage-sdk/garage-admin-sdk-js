/**
 * Garage Administration API v0+garage-v0.9.0
 * Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* 
 *
 * The version of the OpenAPI document: v0.9.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import UpdateKeyRequestAllow from './UpdateKeyRequestAllow';
import UpdateKeyRequestDeny from './UpdateKeyRequestDeny';

/**
 * The UpdateKeyRequest model module.
 * @module model/UpdateKeyRequest
 * @version v0.9.0
 */
class UpdateKeyRequest {
    /**
     * Constructs a new <code>UpdateKeyRequest</code>.
     * @alias module:model/UpdateKeyRequest
     */
    constructor() { 
        
        UpdateKeyRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>UpdateKeyRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateKeyRequest} obj Optional instance to populate.
     * @return {module:model/UpdateKeyRequest} The populated <code>UpdateKeyRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UpdateKeyRequest();

            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('allow')) {
                obj['allow'] = UpdateKeyRequestAllow.constructFromObject(data['allow']);
            }
            if (data.hasOwnProperty('deny')) {
                obj['deny'] = UpdateKeyRequestDeny.constructFromObject(data['deny']);
            }
        }
        return obj;
    }


}

/**
 * @member {String} name
 */
UpdateKeyRequest.prototype['name'] = undefined;

/**
 * @member {module:model/UpdateKeyRequestAllow} allow
 */
UpdateKeyRequest.prototype['allow'] = undefined;

/**
 * @member {module:model/UpdateKeyRequestDeny} deny
 */
UpdateKeyRequest.prototype['deny'] = undefined;






export default UpdateKeyRequest;

