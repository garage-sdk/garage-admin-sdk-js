# garage.BucketApi

All URIs are relative to *http://localhost:3903/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allowBucketKey**](BucketApi.md#allowBucketKey) | **POST** /bucket/allow | Allow key
[**createBucket**](BucketApi.md#createBucket) | **POST** /bucket | Create a bucket
[**deleteBucket**](BucketApi.md#deleteBucket) | **DELETE** /bucket | Delete a bucket
[**deleteBucketGlobalAlias**](BucketApi.md#deleteBucketGlobalAlias) | **DELETE** /bucket/alias/global | Delete a global alias
[**deleteBucketLocalAlias**](BucketApi.md#deleteBucketLocalAlias) | **DELETE** /bucket/alias/local | Delete a local alias
[**denyBucketKey**](BucketApi.md#denyBucketKey) | **POST** /bucket/deny | Deny key
[**getBucketInfo**](BucketApi.md#getBucketInfo) | **GET** /bucket | Get a bucket
[**listBuckets**](BucketApi.md#listBuckets) | **GET** /bucket?list | List all buckets
[**putBucketGlobalAlias**](BucketApi.md#putBucketGlobalAlias) | **PUT** /bucket/alias/global | Add a global alias
[**putBucketLocalAlias**](BucketApi.md#putBucketLocalAlias) | **PUT** /bucket/alias/local | Add a local alias
[**updateBucket**](BucketApi.md#updateBucket) | **PUT** /bucket | Update a bucket



## allowBucketKey

> BucketInfo allowBucketKey(allowBucketKeyRequest)

Allow key

⚠️ **DISCLAIMER**: Garage&#39;s developers are aware that this endpoint has an unconventional semantic. Be extra careful when implementing it, its behavior is not obvious.  Allows a key to do read/write/owner operations on a bucket.  Flags in permissions which have the value true will be activated. Other flags will remain unchanged (ie. they will keep their internal value).  For example, if you set read to true, the key will be allowed to read the bucket. If you set it to false, the key will keeps its previous read permission. If you want to disallow read for the key, check the DenyBucketKey operation. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let allowBucketKeyRequest = new garage.AllowBucketKeyRequest(); // AllowBucketKeyRequest | Aliases to put on the new bucket 
apiInstance.allowBucketKey(allowBucketKeyRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allowBucketKeyRequest** | [**AllowBucketKeyRequest**](AllowBucketKeyRequest.md)| Aliases to put on the new bucket  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## createBucket

> BucketInfo createBucket(createBucketRequest)

Create a bucket

Creates a new bucket, either with a global alias, a local one, or no alias at all. Technically, you can also specify both &#x60;globalAlias&#x60; and &#x60;localAlias&#x60; and that would create two aliases. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let createBucketRequest = new garage.CreateBucketRequest(); // CreateBucketRequest | Aliases to put on the new bucket 
apiInstance.createBucket(createBucketRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createBucketRequest** | [**CreateBucketRequest**](CreateBucketRequest.md)| Aliases to put on the new bucket  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## deleteBucket

> deleteBucket(id)

Delete a bucket

Delete a bucket.Deletes a storage bucket. A bucket cannot be deleted if it is not empty.  **Warning:** this will delete all aliases associated with the bucket! 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let id = b4018dc61b27ccb5c64ec1b24f53454bbbd180697c758c4d47a22a8921864a87; // String | The exact bucket identifier, a 32 bytes hexadecimal string
apiInstance.deleteBucket(id).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The exact bucket identifier, a 32 bytes hexadecimal string | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## deleteBucketGlobalAlias

> BucketInfo deleteBucketGlobalAlias(id, alias)

Delete a global alias

Delete a global alias from the target bucket 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let id = e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b; // String | 
let alias = my_documents; // String | 
apiInstance.deleteBucketGlobalAlias(id, alias).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **alias** | **String**|  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteBucketLocalAlias

> BucketInfo deleteBucketLocalAlias(id, accessKeyId, alias)

Delete a local alias

Delete a local alias, bound to specified account, from the target bucket 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let id = e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b; // String | 
let accessKeyId = GK31c2f218a2e44f485b94239e; // String | 
let alias = my_documents; // String | 
apiInstance.deleteBucketLocalAlias(id, accessKeyId, alias).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **accessKeyId** | **String**|  | 
 **alias** | **String**|  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## denyBucketKey

> BucketInfo denyBucketKey(allowBucketKeyRequest)

Deny key

⚠️ **DISCLAIMER**: Garage&#39;s developers are aware that this endpoint has an unconventional semantic. Be extra careful when implementing it, its behavior is not obvious.  Denies a key from doing read/write/owner operations on a bucket.  Flags in permissions which have the value true will be deactivated. Other flags will remain unchanged.  For example, if you set read to true, the key will be denied from reading. If you set read to false,  the key will keep its previous permissions. If you want the key to have the reading permission, check the AllowBucketKey operation. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let allowBucketKeyRequest = new garage.AllowBucketKeyRequest(); // AllowBucketKeyRequest | Aliases to put on the new bucket 
apiInstance.denyBucketKey(allowBucketKeyRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allowBucketKeyRequest** | [**AllowBucketKeyRequest**](AllowBucketKeyRequest.md)| Aliases to put on the new bucket  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getBucketInfo

> BucketInfo getBucketInfo(opts)

Get a bucket

Given a bucket identifier (&#x60;id&#x60;) or a global alias (&#x60;globalAlias&#x60;), get its information. It includes its aliases, its web configuration, keys that have some permissions on it, some statistics (number of objects, size), number of dangling multipart uploads, and its quotas (if any). 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let opts = {
  'id': b4018dc61b27ccb5c64ec1b24f53454bbbd180697c758c4d47a22a8921864a87, // String | The exact bucket identifier, a 32 bytes hexadecimal string.  Incompatible with `alias`. 
  'globalAlias': my_documents // String | The exact global alias of one of the existing buckets.  Incompatible with `id`. 
};
apiInstance.getBucketInfo(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The exact bucket identifier, a 32 bytes hexadecimal string.  Incompatible with &#x60;alias&#x60;.  | [optional] 
 **globalAlias** | **String**| The exact global alias of one of the existing buckets.  Incompatible with &#x60;id&#x60;.  | [optional] 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## listBuckets

> [ListBuckets200ResponseInner] listBuckets()

List all buckets

List all the buckets on the cluster with their UUID and their global and local aliases. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
apiInstance.listBuckets().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[ListBuckets200ResponseInner]**](ListBuckets200ResponseInner.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## putBucketGlobalAlias

> BucketInfo putBucketGlobalAlias(id, alias)

Add a global alias

Add a global alias to the target bucket 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let id = e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b; // String | 
let alias = my_documents; // String | 
apiInstance.putBucketGlobalAlias(id, alias).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **alias** | **String**|  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## putBucketLocalAlias

> BucketInfo putBucketLocalAlias(id, accessKeyId, alias)

Add a local alias

Add a local alias, bound to specified account, to the target bucket 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let id = e6a14cd6a27f48684579ec6b381c078ab11697e6bc8513b72b2f5307e25fff9b; // String | 
let accessKeyId = GK31c2f218a2e44f485b94239e; // String | 
let alias = my_documents; // String | 
apiInstance.putBucketLocalAlias(id, accessKeyId, alias).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **accessKeyId** | **String**|  | 
 **alias** | **String**|  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateBucket

> BucketInfo updateBucket(id, updateBucketRequest)

Update a bucket

All fields (&#x60;websiteAccess&#x60; and &#x60;quotas&#x60;) are optional. If they are present, the corresponding modifications are applied to the bucket, otherwise nothing is changed.  In &#x60;websiteAccess&#x60;: if &#x60;enabled&#x60; is &#x60;true&#x60;, &#x60;indexDocument&#x60; must be specified. The field &#x60;errorDocument&#x60; is optional, if no error document is set a generic error message is displayed when errors happen. Conversely, if &#x60;enabled&#x60; is &#x60;false&#x60;, neither &#x60;indexDocument&#x60; nor &#x60;errorDocument&#x60; must be specified.  In &#x60;quotas&#x60;: new values of &#x60;maxSize&#x60; and &#x60;maxObjects&#x60; must both be specified, or set to &#x60;null&#x60; to remove the quotas. An absent value will be considered the same as a &#x60;null&#x60;. It is not possible to change only one of the two quotas. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.BucketApi();
let id = b4018dc61b27ccb5c64ec1b24f53454bbbd180697c758c4d47a22a8921864a87; // String | The exact bucket identifier, a 32 bytes hexadecimal string
let updateBucketRequest = new garage.UpdateBucketRequest(); // UpdateBucketRequest | Requested changes on the bucket. Both root fields are optionals. 
apiInstance.updateBucket(id, updateBucketRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The exact bucket identifier, a 32 bytes hexadecimal string | 
 **updateBucketRequest** | [**UpdateBucketRequest**](UpdateBucketRequest.md)| Requested changes on the bucket. Both root fields are optionals.  | 

### Return type

[**BucketInfo**](BucketInfo.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

