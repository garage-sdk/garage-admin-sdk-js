/**
 * Garage Administration API v0+garage-v0.9.0
 * Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* 
 *
 * The version of the OpenAPI document: v0.9.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The NodeRoleUpdate model module.
 * @module model/NodeRoleUpdate
 * @version v0.9.0
 */
class NodeRoleUpdate {
    /**
     * Constructs a new <code>NodeRoleUpdate</code>.
     * @alias module:model/NodeRoleUpdate
     * @param id {String} 
     * @param zone {String} 
     * @param capacity {Number} 
     * @param tags {Array.<String>} 
     */
    constructor(id, zone, capacity, tags) { 
        
        NodeRoleUpdate.initialize(this, id, zone, capacity, tags);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, id, zone, capacity, tags) { 
        obj['id'] = id;
        obj['zone'] = zone;
        obj['capacity'] = capacity;
        obj['tags'] = tags;
    }

    /**
     * Constructs a <code>NodeRoleUpdate</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NodeRoleUpdate} obj Optional instance to populate.
     * @return {module:model/NodeRoleUpdate} The populated <code>NodeRoleUpdate</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new NodeRoleUpdate();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'String');
            }
            if (data.hasOwnProperty('zone')) {
                obj['zone'] = ApiClient.convertToType(data['zone'], 'String');
            }
            if (data.hasOwnProperty('capacity')) {
                obj['capacity'] = ApiClient.convertToType(data['capacity'], 'Number');
            }
            if (data.hasOwnProperty('tags')) {
                obj['tags'] = ApiClient.convertToType(data['tags'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {String} id
 */
NodeRoleUpdate.prototype['id'] = undefined;

/**
 * @member {String} zone
 */
NodeRoleUpdate.prototype['zone'] = undefined;

/**
 * @member {Number} capacity
 */
NodeRoleUpdate.prototype['capacity'] = undefined;

/**
 * @member {Array.<String>} tags
 */
NodeRoleUpdate.prototype['tags'] = undefined;






export default NodeRoleUpdate;

