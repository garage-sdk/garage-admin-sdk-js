# garage.NodeRoleRemove

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**remove** | **Boolean** |  | 


