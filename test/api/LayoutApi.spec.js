/**
 * Garage Administration API v0+garage-v0.8.0
 * Administrate your Garage cluster programatically, including status, layout, keys, buckets, and maintainance tasks.  *Disclaimer: The API is not stable yet, hence its v0 tag. The API can change at any time, and changes can include breaking backward compatibility. Read the changelog and upgrade your scripts before upgrading. Additionnaly, this specification is very early stage and can contain bugs, especially on error return codes/types that are not tested yet. Do not expect a well finished and polished product!* 
 *
 * The version of the OpenAPI document: v0.8.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.garage);
  }
}(this, function(expect, garage) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new garage.LayoutApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('LayoutApi', function() {
    describe('addLayout', function() {
      it('should call addLayout successfully', function(done) {
        //uncomment below and update the code to test addLayout
        //instance.addLayout(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('applyLayout', function() {
      it('should call applyLayout successfully', function(done) {
        //uncomment below and update the code to test applyLayout
        //instance.applyLayout(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getLayout', function() {
      it('should call getLayout successfully', function(done) {
        //uncomment below and update the code to test getLayout
        //instance.getLayout(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('revertLayout', function() {
      it('should call revertLayout successfully', function(done) {
        //uncomment below and update the code to test revertLayout
        //instance.revertLayout(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
