# garage.BucketKeyInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessKeyId** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**permissions** | [**CreateBucketRequestLocalAliasAllow**](CreateBucketRequestLocalAliasAllow.md) |  | [optional] 
**bucketLocalAliases** | **[String]** |  | [optional] 


