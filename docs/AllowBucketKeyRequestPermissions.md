# garage.AllowBucketKeyRequestPermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read** | **Boolean** |  | 
**write** | **Boolean** |  | 
**owner** | **Boolean** |  | 


