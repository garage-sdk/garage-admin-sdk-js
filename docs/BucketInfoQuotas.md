# garage.BucketInfoQuotas

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxSize** | **Number** |  | [optional] 
**maxObjects** | **Number** |  | [optional] 


