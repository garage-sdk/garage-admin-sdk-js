# garage.UpdateBucketRequestWebsiteAccess

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** |  | [optional] 
**indexDocument** | **String** |  | [optional] 
**errorDocument** | **String** |  | [optional] 


