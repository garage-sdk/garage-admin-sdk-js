# garage.CreateBucketRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**globalAlias** | **String** |  | [optional] 
**localAlias** | [**CreateBucketRequestLocalAlias**](CreateBucketRequestLocalAlias.md) |  | [optional] 


