# garage.ImportKeyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**accessKeyId** | **String** |  | 
**secretAccessKey** | **String** |  | 


