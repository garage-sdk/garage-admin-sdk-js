# garage.ListBuckets200ResponseInnerLocalAliasesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alias** | **String** |  | 
**accessKeyId** | **String** |  | 


