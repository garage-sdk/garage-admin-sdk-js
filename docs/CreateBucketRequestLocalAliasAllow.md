# garage.CreateBucketRequestLocalAliasAllow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read** | **Boolean** |  | [optional] 
**write** | **Boolean** |  | [optional] 
**owner** | **Boolean** |  | [optional] 


