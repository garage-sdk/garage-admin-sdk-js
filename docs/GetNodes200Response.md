# garage.GetNodes200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node** | **String** |  | 
**garageVersion** | **String** |  | 
**garageFeatures** | **[String]** |  | 
**rustVersion** | **String** |  | 
**dbEngine** | **String** |  | 
**knownNodes** | [**[NodeNetworkInfo]**](NodeNetworkInfo.md) |  | 
**layout** | [**ClusterLayout**](ClusterLayout.md) |  | 


