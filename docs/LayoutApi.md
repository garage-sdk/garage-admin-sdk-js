# garage.LayoutApi

All URIs are relative to *http://localhost:3903/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addLayout**](LayoutApi.md#addLayout) | **POST** /layout | Send modifications to the cluster layout
[**applyLayout**](LayoutApi.md#applyLayout) | **POST** /layout/apply | Apply staged layout
[**getLayout**](LayoutApi.md#getLayout) | **GET** /layout | Details on the current and staged layout
[**revertLayout**](LayoutApi.md#revertLayout) | **POST** /layout/revert | Clear staged layout



## addLayout

> ClusterLayout addLayout(nodeRoleChange)

Send modifications to the cluster layout

Send modifications to the cluster layout. These modifications will be included in the staged role changes, visible in subsequent calls of &#x60;GET /layout&#x60;. Once the set of staged changes is satisfactory, the user may call &#x60;POST /layout/apply&#x60; to apply the changed changes, or &#x60;POST /layout/revert&#x60; to clear all of the staged changes in the layout.  Setting the capacity to &#x60;null&#x60; will configure the node as a gateway. Otherwise, capacity must be now set in bytes (before Garage 0.9 it was arbitrary weights). For example to declare 100GB, you must set &#x60;capacity: 100000000000&#x60;.  Garage uses internally the International System of Units (SI), it assumes that 1kB &#x3D; 1000 bytes, and displays storage as kB, MB, GB (and not KiB, MiB, GiB that assume 1KiB &#x3D; 1024 bytes). 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.LayoutApi();
let nodeRoleChange = [new garage.NodeRoleChange()]; // [NodeRoleChange] | To add a new node to the layout or to change the configuration of an existing node, simply set the values you want (`zone`, `capacity`, and `tags`). To remove a node, simply pass the `remove: true` field. This logic is represented in OpenAPI with a \"One Of\" object.  Contrary to the CLI that may update only a subset of the fields capacity, zone and tags, when calling this API all of these values must be specified. 
apiInstance.addLayout(nodeRoleChange).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nodeRoleChange** | [**[NodeRoleChange]**](NodeRoleChange.md)| To add a new node to the layout or to change the configuration of an existing node, simply set the values you want (&#x60;zone&#x60;, &#x60;capacity&#x60;, and &#x60;tags&#x60;). To remove a node, simply pass the &#x60;remove: true&#x60; field. This logic is represented in OpenAPI with a \&quot;One Of\&quot; object.  Contrary to the CLI that may update only a subset of the fields capacity, zone and tags, when calling this API all of these values must be specified.  | 

### Return type

[**ClusterLayout**](ClusterLayout.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## applyLayout

> ApplyLayout200Response applyLayout(layoutVersion)

Apply staged layout

Applies to the cluster the layout changes currently registered as staged layout changes.  *Note: do not try to parse the &#x60;message&#x60; field of the response, it is given as an array of string specifically because its format is not stable.* 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.LayoutApi();
let layoutVersion = new garage.LayoutVersion(); // LayoutVersion | Similarly to the CLI, the body must include the version of the new layout that will be created, which MUST be 1 + the value of the currently existing layout in the cluster. 
apiInstance.applyLayout(layoutVersion).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layoutVersion** | [**LayoutVersion**](LayoutVersion.md)| Similarly to the CLI, the body must include the version of the new layout that will be created, which MUST be 1 + the value of the currently existing layout in the cluster.  | 

### Return type

[**ApplyLayout200Response**](ApplyLayout200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getLayout

> ClusterLayout getLayout()

Details on the current and staged layout

Returns the cluster&#39;s current layout, including:   - Currently configured cluster layout   - Staged changes to the cluster layout    *Capacity is given in bytes* *The info returned by this endpoint is a subset of the info returned by &#x60;GET /status&#x60;.* 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.LayoutApi();
apiInstance.getLayout().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ClusterLayout**](ClusterLayout.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## revertLayout

> revertLayout(layoutVersion)

Clear staged layout

Clears all of the staged layout changes. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.LayoutApi();
let layoutVersion = new garage.LayoutVersion(); // LayoutVersion | Reverting the staged changes is done by incrementing the version number and clearing the contents of the staged change list. Similarly to the CLI, the body must include the incremented version number, which MUST be 1 + the value of the currently existing layout in the cluster. 
apiInstance.revertLayout(layoutVersion).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **layoutVersion** | [**LayoutVersion**](LayoutVersion.md)| Reverting the staged changes is done by incrementing the version number and clearing the contents of the staged change list. Similarly to the CLI, the body must include the incremented version number, which MUST be 1 + the value of the currently existing layout in the cluster.  | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

