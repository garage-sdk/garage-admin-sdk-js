# garage.NodeRoleUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**zone** | **String** |  | 
**capacity** | **Number** |  | 
**tags** | **[String]** |  | 


