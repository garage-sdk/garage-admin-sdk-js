# garage.CreateBucketRequestLocalAlias

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessKeyId** | **String** |  | [optional] 
**alias** | **String** |  | [optional] 
**allow** | [**CreateBucketRequestLocalAliasAllow**](CreateBucketRequestLocalAliasAllow.md) |  | [optional] 


