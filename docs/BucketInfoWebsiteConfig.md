# garage.BucketInfoWebsiteConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**indexDocument** | **String** |  | [optional] 
**errorDocument** | **String** |  | [optional] 


