# garage.ClusterLayout

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **Number** |  | 
**roles** | [**[NodeClusterInfo]**](NodeClusterInfo.md) |  | 
**stagedRoleChanges** | [**[NodeRoleChange]**](NodeRoleChange.md) |  | 


