# garage.KeyInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**accessKeyId** | **String** |  | [optional] 
**secretAccessKey** | **String** |  | [optional] 
**permissions** | [**KeyInfoPermissions**](KeyInfoPermissions.md) |  | [optional] 
**buckets** | [**[KeyInfoBucketsInner]**](KeyInfoBucketsInner.md) |  | [optional] 


