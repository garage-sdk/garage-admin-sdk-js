# garage.NodesApi

All URIs are relative to *http://localhost:3903/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addNode**](NodesApi.md#addNode) | **POST** /connect | Connect a new node
[**getHealth**](NodesApi.md#getHealth) | **GET** /health | Cluster health report
[**getNodes**](NodesApi.md#getNodes) | **GET** /status | Describe cluster



## addNode

> [AddNode200ResponseInner] addNode(requestBody)

Connect a new node

Instructs this Garage node to connect to other Garage nodes at specified &#x60;&lt;node_id&gt;@&lt;net_address&gt;&#x60;. &#x60;node_id&#x60; is generated automatically on node start. 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.NodesApi();
let requestBody = ["null"]; // [String] | 
apiInstance.addNode(requestBody).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**[String]**](String.md)|  | 

### Return type

[**[AddNode200ResponseInner]**](AddNode200ResponseInner.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getHealth

> GetHealth200Response getHealth()

Cluster health report

Returns the global status of the cluster, the number of connected nodes (over the number of known ones), the number of healthy storage nodes (over the declared ones), and the number of healthy partitions (over the total). 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.NodesApi();
apiInstance.getHealth().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**GetHealth200Response**](GetHealth200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getNodes

> GetNodes200Response getNodes()

Describe cluster

Returns the cluster&#39;s current status, including:  - ID of the node being queried and its version of the Garage daemon  - Live nodes  - Currently configured cluster layout  - Staged changes to the cluster layout  *Capacity is given in bytes* 

### Example

```javascript
import garage from 'garage_administration_api_v0garage_v0_9_0';
let defaultClient = garage.ApiClient.instance;
// Configure Bearer access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new garage.NodesApi();
apiInstance.getNodes().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**GetNodes200Response**](GetNodes200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

