# garage.NodeNetworkInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**addr** | **String** |  | 
**isUp** | **Boolean** |  | 
**lastSeenSecsAgo** | **Number** |  | 
**hostname** | **String** |  | 


