# garage.GetHealth200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**knownNodes** | **Number** |  | 
**connectedNodes** | **Number** |  | 
**storageNodes** | **Number** |  | 
**storageNodesOk** | **Number** |  | 
**partitions** | **Number** |  | 
**partitionsQuorum** | **Number** |  | 
**partitionsAllOk** | **Number** |  | 


