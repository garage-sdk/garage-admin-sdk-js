# garage.ApplyLayout200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **[String]** |  | 
**layout** | [**ClusterLayout**](ClusterLayout.md) |  | 


