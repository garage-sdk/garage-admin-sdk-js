# garage.KeyInfoBucketsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**globalAliases** | **[String]** |  | [optional] 
**localAliases** | **[String]** |  | [optional] 
**permissions** | [**KeyInfoBucketsInnerPermissions**](KeyInfoBucketsInnerPermissions.md) |  | [optional] 


