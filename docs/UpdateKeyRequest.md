# garage.UpdateKeyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**allow** | [**UpdateKeyRequestAllow**](UpdateKeyRequestAllow.md) |  | [optional] 
**deny** | [**UpdateKeyRequestDeny**](UpdateKeyRequestDeny.md) |  | [optional] 


