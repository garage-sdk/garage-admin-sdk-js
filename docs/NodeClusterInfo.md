# garage.NodeClusterInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zone** | **String** |  | 
**capacity** | **Number** |  | [optional] 
**tags** | **[String]** | User defined tags, put whatever makes sense for you, these tags are not interpreted by Garage  | 


