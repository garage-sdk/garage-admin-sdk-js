# garage.ListBuckets200ResponseInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**globalAliases** | **[String]** |  | [optional] 
**localAliases** | [**[ListBuckets200ResponseInnerLocalAliasesInner]**](ListBuckets200ResponseInnerLocalAliasesInner.md) |  | [optional] 


