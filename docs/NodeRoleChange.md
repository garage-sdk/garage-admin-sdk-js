# garage.NodeRoleChange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**remove** | **Boolean** |  | 
**zone** | **String** |  | 
**capacity** | **Number** |  | 
**tags** | **[String]** |  | 


